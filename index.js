import React, {Fragment} from 'react'
import {View, Platform } from 'react-native';
import RNShadowAndroid from './RNShadowAndroid';


class ShadowView extends React.Component{

    constructor (props){
        super(props)
        this.state = {
            width: 0,
            height: 0
        }
    }

    render(){
        return(
          <Fragment>
              {
                  Platform.OS==='android'?
                    <View
                      onLayout={(e)=>{
                          this.setState({
                              width: e.nativeEvent.layout.width,
                              height: e.nativeEvent.layout.height
                          })
                      }}
                      style={[{
                      },
                          this.props.style? (this.props.style.flex?{flex: this.props.style.flex} : {}):{},
                          this.props.style? (this.props.style.marginVertical?{marginVertical: this.props.style.marginVertical} : {}):{},
                          this.props.style? (this.props.style.marginHorizontal?{marginHorizontal: this.props.style.marginHorizontal} : {}):{},
                          this.props.style? (this.props.style.borderRadius?{borderRadius: this.props.style.borderRadius} : {}):{}
                      ]}
                    >
                        <RNShadowAndroid
                          style={[this.props.style,{
                              backgroundColor: 'transparent',
                              width: this.props.style.width?this.props.style.width:this.state.width,
                              height: this.state.height,
                              marginHorizontal: 0,
                              padding: 0,
                              position: 'absolute'}]}
                          shadowAngle={this.props.style?(this.props.style.shadowAngle?this.props.style.shadowAngle:0):0}
                          shadowColor={this.props.style?(this.props.style?.shadowColor?this.props.style?.shadowColor?.toString(): "#0000002b"):"#0000002b"}
                          shadowDistance={this.props.style?(this.props.style.shadowDistance?this.props.style.shadowDistance:0):0}
                          shadowRadius={this.props.style?(this.props.style.shadowRadius?this.props.style.shadowRadius:0):0}
                        >
                            <View style={{
                                backgroundColor:'white',
                                borderRadius:this.props.style.borderRadius,
                                // width: this.state.width,
                                // height: this.state.height,
                            }} />

                        </RNShadowAndroid>
                        <View style={[{ backgroundColor: 'transparent'}, this.props.style?(this.props.style.flex?{flex: this.props.style.flex}: {}):{}]}>
                                {
                                    this.props.children
                                }
                        </View>
                    </View>
                    :
                    <View style={[ this.props.style?(this.props.style.flex?{flex: this.props.style.flex}: {}):{}]}>
                        <View {...this.props} style={[this.props.style,{margin: this.props.style.shadowRadius}]} />
                    </View>
              }

          </Fragment>
        )
    }

}

export default ShadowView;
